import React, { useState, useEffect } from "react";
import { fetchWeather } from "../../actions/weatherAction";
import SearchBar from "../common/SearchBar";
import DropdownList from "../common/DropdownList";
import { ApiKey } from "../../config/config.json";
import { Wrapper, FilterWrapper } from "./style";

const Filter = ({ onApiCall }) => {
  const [filterSelected, setFilterSelected] = useState("");
  const [dropDownItems, setDropDownItems] = useState([]);
  const [searchQuery, setSearchQuery] = useState("");

  useEffect(() => {
    const dropDownItemsList = [
      { id: "", name: "Search By" },
      { id: "q", name: "City Name" },
      { id: "id", name: "Zip Code" },
    ];
    setDropDownItems(dropDownItemsList);
  }, []);

  const handleSearchQueryChange = ({ currentTarget: input }) => {
    const { value } = input;
    setSearchQuery(value);
  };

  const handleFilterChange = ({ currentTarget: input }) => {
    const { value } = input;
    setFilterSelected(value);
  };

  const submit = async () => {
    if (!filterSelected || !searchQuery) {
      alert("Invalid Values");
      return;
    }
    const reqData = fetchWeather({
      searchQuery,
      filterSelected,
      ApiKey,
    });
    await onApiCall(reqData);
  };

  return (
    <Wrapper>
      <FilterWrapper>
        <DropdownList
          name="filterSelected"
          value={filterSelected}
          options={dropDownItems}
          onFilterChange={handleFilterChange}
        />
        <SearchBar
          name="searchQuery"
          value={searchQuery}
          onChange={handleSearchQueryChange}
          onClick={submit}
        />
      </FilterWrapper>
    </Wrapper>
  );
};

export default Filter;
