import styled from "styled-components";

export const Wrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(4, 2fr);
  grid-template-rows: 1fr;
`;

export const FilterWrapper = styled.div`
  grid-column: 2/3;
  display: flex;
  flex-direct: row;
  padding: 20px;
`;
