import React, { useRef, useEffect } from "react";
import { Wrapper, CustomIcon, CustomInput } from "./style";

const SearchBar = ({ name, value, onChange, onClick }) => {
  const inputRef = useRef(null);

  useEffect(() => {
    inputRef.current.focus();
  }, []);
  return (
    <Wrapper>
      <CustomInput
        ref={inputRef}
        type="text"
        name={name}
        key={name}
        placeholder="Search Item..."
        value={value}
        onChange={(e) => onChange(e)}
      />
      <CustomIcon
        onClick={onClick}
        className="fa fa-search"
        aria-hidden="true"
      ></CustomIcon>
    </Wrapper>
  );
};

export default SearchBar;
