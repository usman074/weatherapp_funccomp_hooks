import styled, { css } from "styled-components";

export const Wrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(4, 2fr);
  grid-template-rows: auto;
`;

export const CityWrapper = styled.div`
  grid-column: 2/3;
  color: #929396;
`;

export const CityDetail = styled.h3`
  margin: 0px;
`;

export const OtherTemperatureFactors = styled.div`
  grid-column: 3/4;
  color: #929396;
`;

export const TemperatureDetail = styled.div`
  grid-column: 2/3;
  display: flex;
  flex-direction: row;
`;

export const Scale = styled.h4`
  color: #929396;
  cursor: pointer;
`;

export const ForecastPanelWrapper = styled.div`
  grid-column: 2/4;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  color: #393b40;
`;

export const AverageTemp = styled.div`
  font-size: 12px;
  display: flex;
  flex-direction: row;
  justify-content: center;
`;

export const AreaChartWrapper = styled.div`
  grid-column: 2/4;
`;

export const ResponsiveImage = styled.img`
  width: 100%;
  height: auto;
  max-width: 115px;
`;

export const SelectedDayStyles = css`
  border: ${(props) => (props.selected ? "1px solid grey" : "none")};
  border-radius: ${(props) => (props.selected ? "5px" : "none")};
  background-color: ${(props) => (props.selected ? "#c3bbbbbf" : "none")};
  width: 100%;
  height: 100%;
  margin-right: 15px;
  text-align: center;
  cursor: pointer;
`;
