import React, { useState, useEffect } from "react";
import styled from "styled-components";
import AreaChart from "../common/AreaChart";

import {
  Wrapper,
  CityWrapper,
  CityDetail,
  OtherTemperatureFactors,
  TemperatureDetail,
  Scale,
  ForecastPanelWrapper,
  SelectedDayStyles,
  AverageTemp,
  ResponsiveImage,
  AreaChartWrapper,
} from "./style";
import {
  getDays,
  groupDataByDate,
  getAverageWeather,
  getIcon,
  getSelectionStyle,
  weatherDetail,
} from "./utils";

const WeatherDetails = ({ onApiResponse }) => {
  // const weatherContext = useContext(WeatherContext);
  const [days] = useState([
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
    "Sunday",
  ]);
  const [groupedWeatherByDate, setGroupedWeatherByDate] = useState({});
  const [selectedDay, setSelectedDay] = useState(0);
  const [scale, setScale] = useState(true);
  const [weather, setWeather] = useState({});
  const [sortedDays, setSortedDays] = useState([]);
  const [mappedDays, setMappedDays] = useState([]);

  const { response: weatherList, error } = onApiResponse;

  useEffect(() => {
    const { sortedDays, mappedDays } = getDays(days);
    setSortedDays(sortedDays);
    setMappedDays(mappedDays);
  }, [days]);

  useEffect(() => {
    if (error) {
      alert(error);
    }
    if (weatherList && weatherList.list && weatherList.list.length > 0) {
      const groupedWeatherByDateChanged = groupDataByDate(weatherList.list);
      setGroupedWeatherByDate(groupedWeatherByDateChanged);
    }
    console.log("effect1");
  }, [weatherList, error]);

  useEffect(() => {
    if (Object.keys(groupedWeatherByDate).length > 0) {
      const weatherDetailObj = weatherDetail(
        selectedDay,
        Object.keys(mappedDays[selectedDay]),
        groupedWeatherByDate
      );
      setWeather(weatherDetailObj);
    }
    console.log("effect2");
  }, [groupedWeatherByDate, selectedDay, mappedDays]);

  const handleSelectedDay = (index) => {
    const newSelectedDay = index;
    setSelectedDay(newSelectedDay);
  };

  const handleTempScale = () => {
    const scaleChanged = !scale;
    setScale(scaleChanged);
  };

  const SelectedDay = styled.div`
    ${SelectedDayStyles}
  `;

  if (
    Object.keys(groupedWeatherByDate).length === 0 ||
    Object.keys(weatherList).length === 0
  )
    return null;

  const { city } = weatherList;

  return (
    <Wrapper>
      <CityWrapper>
        <CityDetail>
          {city.name} <span>,{city.country}</span>
        </CityDetail>
        <div>{sortedDays[selectedDay]}</div>
        <div>{weather.weather_desc}</div>
      </CityWrapper>
      <TemperatureDetail>
        <ResponsiveImage
          src={getIcon(
            Object.keys(mappedDays[selectedDay]),
            groupedWeatherByDate
          )}
          alt="weather-icon"
        />
        <h1>
          {scale ? weather.avg_temp_centigrade : weather.avg_temp_fahrenheit}
        </h1>
        <Scale onClick={handleTempScale}>
          <sup>&#176; C | </sup>
          <sup>&#176; F</sup>
        </Scale>
      </TemperatureDetail>
      <OtherTemperatureFactors>
        <div>
          <p>Pressure: {weather.avg_pressure} hPa</p>
          <p>Humidity: {weather.avg_humidity}%</p>
          <p>Wind Speed: {weather.avg_wind} m/s</p>
        </div>
      </OtherTemperatureFactors>

      <ForecastPanelWrapper>
        {sortedDays.map((day, index) => (
          <SelectedDay
            selected={getSelectionStyle(selectedDay, index)}
            key={index}
            onClick={() => handleSelectedDay(index)}
          >
            <p>{day}</p>
            <img
              src={getIcon(
                Object.keys(mappedDays[index]),
                groupedWeatherByDate
              )}
              alt="weather-icon"
            />
            <AverageTemp>
              <p>
                <strong>
                  {getAverageWeather(
                    Object.keys(mappedDays[index]),
                    groupedWeatherByDate,
                    1,
                    "temp_max"
                  )}
                  <span>&#176;</span>
                </strong>
              </p>

              <p>
                {getAverageWeather(
                  Object.keys(mappedDays[index]),
                  groupedWeatherByDate,
                  1,
                  "temp_min"
                )}
                <span>&#176;</span>
              </p>
            </AverageTemp>
          </SelectedDay>
        ))}
      </ForecastPanelWrapper>

      <AreaChartWrapper>
        <AreaChart
          weatherData={groupedWeatherByDate}
          date={Object.keys(mappedDays[selectedDay])}
        />
      </AreaChartWrapper>
    </Wrapper>
  );
};

export default WeatherDetails;
