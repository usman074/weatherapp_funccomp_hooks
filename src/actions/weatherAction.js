import { urls } from "../utils/urls";

const { weatherUrl } = urls;

export const fetchWeather = ({ filterSelected, searchQuery, ApiKey }) => {
  return {
    url: `${weatherUrl}?${filterSelected}=${searchQuery}&units=metric&appid=${ApiKey}`,
    options: {
      method: "get",
    },
    payload: {},
    meta: { type: "api" },
  };
};
