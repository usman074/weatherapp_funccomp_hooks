import React from "react";
// import logo from "./logo.svg";
import "./App.css";
import Filter from "./components/Filter";
import Header from "./components/header";
import WeatherDetails from "./components/WeatherDetails";
import useApiCall from "./customHooks/apiCallHook";

function App() {
  const [weatherState, callApi] = useApiCall();

  return (
    <div>
      <Header />
      <Filter onApiCall={callApi} />
      <WeatherDetails onApiResponse={weatherState} />
    </div>
  );
}

export default App;
