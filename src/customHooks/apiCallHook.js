import { useReducer } from "react";
import http from "../services/httpService";
import { reducer, initialState } from "../reducers";

export const API_REQUEST = "API_REQUEST";
export const API_REQUEST_SUCCESS = "API_REQUEST_SUCCESS";
export const API_REQUEST_FAILURE = "API_REQUEST_FAILURE";

function useApiCall(state = {}) {
  const [State, dispatch] = useReducer(reducer, {
    ...initialState,
    ...state,
  });

  const callApi = async (obj) => {
    const { url, options, payload } = obj;

    if (typeof obj === "undefined") {
      return null;
    }

    if (!obj.meta || obj.meta.type !== "api") {
      return null;
    }
    let response;
    if (options.method === "get") {
      try {
        response = await http.axios({
          method: options.method,
          url: url,
        });
        const newAction = {
          type: API_REQUEST_SUCCESS,
          payload: response,
        };
        dispatch(newAction);
      } catch (error) {
        const newAction = {
          type: API_REQUEST_FAILURE,
          payload: error,
        };
        dispatch(newAction);
      }
    } else {
      try {
        response = await http.axios({
          method: options.method,
          url: url,
          data: payload,
        });
        const newAction = {
          type: API_REQUEST_SUCCESS,
          payload: response,
        };
        dispatch(newAction);
      } catch (error) {
        const newAction = {
          type: API_REQUEST_FAILURE,
          payload: error,
        };
        dispatch(newAction);
      }
    }
  };

  return [State, callApi];
}

export default useApiCall;
