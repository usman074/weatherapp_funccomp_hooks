import { API_REQUEST_SUCCESS } from "../customHooks/apiCallHook";
import { API_REQUEST_FAILURE } from "../customHooks/apiCallHook";

export const initialState = {
  response: {},
  error: "",
};

export function reducer(state = initialState, action) {
  switch (action.type) {
    case API_REQUEST_SUCCESS:
      return {
        ...state,
        response: action.payload.data,
        error: null,
      };
    case API_REQUEST_FAILURE:
      return {
        ...state,
        response: {},
        error: action.payload.response.data.message,
      };
    default:
      return state;
  }
}
